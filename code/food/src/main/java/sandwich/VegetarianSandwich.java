package sandwich;

public abstract class VegetarianSandwich implements ISandwich {
    private String filling;


    /**
     * This is a constructor, intializes filling to empty string
     * 
     */
    public VegetarianSandwich(){
        this.filling = "";
    }

    /**
     * This method appends to the field filling with the toppings. If contains 
     * meat toppings, throws IllegalArgumentException
     * @param topping - Represents the toppings of the sandwich 
     */
    public void addFilling(String topping){
        String[] meats = {"chicken", "beef", "fish", "meat", "pork"};
        for(int i = 0; i < meats.length; i++)
        {
            if (topping.contains(meats[i]))
            {
                throw new IllegalArgumentException("You stink");
            }
        }
        this.filling += topping + ", ";
    }

    public String getFilling()
    {
        return(this.filling);
    }

    /**
     * This method returns true.
     * @return - returns boolean that is true.
     */
    public final boolean isVegetarian(){
        return true;
    }



    public abstract String getProtein();

    /**
     * This method returns true or false if filling contains "Cheese" or "egg".
     * @return - returns boolean representing if sandwich is vegan or not.
     */
    public boolean isVegan(){
        if(this.filling.contains("cheese") || this.filling.contains("egg")){
            return false;
        }
        return true;
        //vegan
    }
}
