package sandwich;

public class TofuSandwich extends VegetarianSandwich {

    public TofuSandwich(){
        addFilling("Tofu");
    }
    
    @Override
    public String getProtein(){
        return "Tofu";
    }
}
