package sandwich;

public class Application {
    public static void main(String[] args){
        ISandwich s = new TofuSandwich();

        if(s instanceof ISandwich){
            System.out.println("yes it is ISandwich");
        }
        else {
            System.out.println("no it aint");
        }

        if(s instanceof VegetarianSandwich){
            System.out.println("yes it is vege sandwich");
        }
        else {
            System.out.println("no it aint");
        }

        //s.addFilling("chicken");
        //s.isVegan();

        VegetarianSandwich veggie = (VegetarianSandwich) s;
        veggie.isVegan();

        VegetarianSandwich sandwich = new CaesarSandwich();
        System.out.println(sandwich.isVegetarian());
    }
}