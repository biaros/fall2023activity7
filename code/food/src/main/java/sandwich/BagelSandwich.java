package sandwich;
// bianca rossetti
public class BagelSandwich implements ISandwich {
    private String filling;

    /**
     * this constructor initializes the filling to be empty
     */
    public BagelSandwich()
    {
        this.filling = "";
    }

    /**
     * appends the topping to the filling String field
     * @param topping - represents the new topping
     */
    public void addFilling(String topping)
    {
        this.filling = this.filling + topping + ", ";
    }

    /**
     * this method will return the value of the filling field
     * @return - returns the value of the filling field
     */
    public String getFilling()
    {
        return(this.filling);
    }

    /** 
     * this method throws an UnsupportedOperationException
     */
    public boolean isVegetarian()
    {
        throw new UnsupportedOperationException("Not vegetarian sandwich!");
    }

}
