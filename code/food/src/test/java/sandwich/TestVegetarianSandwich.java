package sandwich;
import static org.junit.Assert.*;
import org.junit.Test;
public class TestVegetarianSandwich {

    // using tofu sandwich
    @Test 
    public void testConstructor_shouldReturnEmpty()
    {
        VegetarianSandwich s = new TofuSandwich();
        assertEquals("Tofu, ", s.getFilling());
    }

    @Test
    public void testAddFilling_shouldReturnCucumber()
    {
        VegetarianSandwich s = new TofuSandwich();
        s.addFilling("cucumber");
        assertEquals("Tofu, cucumber, ", s.getFilling());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddFilling_shouldThrowException()
    {
        VegetarianSandwich s = new TofuSandwich();
        s.addFilling("chicken");
    }

    @Test
    public void isVegetarian_shouldReturnTrue()
    {
        VegetarianSandwich s = new TofuSandwich();
        assertTrue(s.isVegetarian());
    }

    @Test
    public void isVegan_shouldReturnTrue()
    {
        VegetarianSandwich s = new TofuSandwich();
        s.addFilling("cucumber");
        s.addFilling("hummus");
        s.addFilling("lettuce");
        assertTrue(s.isVegan());
    }

    @Test
    public void isVegan_shouldReturnFalse()
    {
        VegetarianSandwich s = new TofuSandwich();
        s.addFilling("cucumber");
        s.addFilling("egg");
        s.addFilling("lettuce");
        assertFalse(s.isVegan());
    }

}
