package sandwich;
import static org.junit.Assert.*;
import org.junit.Test;

public class TestBagel {
    
    @Test
    public void constructorIsReallyEmptyYes(){
        BagelSandwich b = new BagelSandwich();
        assertEquals("", b.getFilling());
    }

    @Test 
    public void addFilling(){
        BagelSandwich b = new BagelSandwich();
        b.addFilling("tomato");
        assertEquals("tomato, ", b.getFilling());
    }

    @Test 
    public void addMultipleFilling(){
        BagelSandwich b = new BagelSandwich();
        b.addFilling("tomato");
        b.addFilling("hummus");
        b.addFilling("salmon");
        assertEquals("tomato, hummus, salmon, ", b.getFilling());
    }

    @Test (expected = UnsupportedOperationException.class)
    public void shouldReturnException(){
        BagelSandwich b = new BagelSandwich();
        b.isVegetarian();
    }

}
